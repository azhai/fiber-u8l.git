// ⚡️ Fiber is an Express inspired web framework written in Go with ☕️
// 🤖 Github Repository: https://gitee.com/azhai/fiber-u8l
// 📌 API Documentation: https://docs.gofiber.io

package utils

import (
	"testing"
)

func Test_AssertEqual(t *testing.T) {
	t.Parallel()
	AssertEqual(nil, []string{}, []string{})
	AssertEqual(t, []string{}, []string{})
}
